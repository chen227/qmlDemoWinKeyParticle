﻿#include "querykeythread.h"
#include <QDebug>

QString key_value[256] = {
    "0",
    "LBUTTON",
    "RBUTTON",
    "CANCEL",
    "MBUTTON",
    "XBUTTON1",
    "XBUTTON2",
    "07",
    "BACK",
    "TAB",
    "10",
    "11",
    "CLEAR",
    "RETURN",
    "14",
    "15",
    "SHIFT",
    "CONTROL",
    "MENU",
    "PAUSE",
    "CAPITAL",
    "KANA",
    "22",
    "JUNJA",
    "FINAL",
    "KANJI",
    "26",
    "ESCAPE",
    "CONVERT",
    "NONCONVERT",
    "ACCEPT",
    "MODECHANGE",
    "SPACE",
    "PRIOR",
    "NEXT",
    "END",
    "HOME",
    "LEFT",
    "UP",
    "RIGHT",
    "DOWN",
    "SELECT",
    "PRINT",
    "EXECUTE",
    "SNAPSHOT",
    "INSERT",
    "DELETE",
    "HELP",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "58",
    "59",
    "60",
    "61",
    "62",
    "63",
    "64",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "LWIN",
    "RWIN",
    "APPS",
    "94",
    "SLEEP",
    "NUMPAD0",
    "NUMPAD1",
    "NUMPAD2",
    "NUMPAD3",
    "NUMPAD4",
    "NUMPAD5",
    "NUMPAD6",
    "NUMPAD7",
    "NUMPAD8",
    "NUMPAD9",
    "MULTIPLY",
    "ADD",
    "SEPARATOR",
    "SUBTRACT",
    "DECIMAL",
    "DIVIDE",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",
    "F13",
    "F14",
    "F15",
    "F16",
    "F17",
    "F18",
    "F19",
    "F20",
    "F21",
    "F22",
    "F23",
    "F24",
    "136",
    "137",
    "138",
    "139",
    "140",
    "141",
    "142",
    "143",
    "NUMLOCK",
    "SCROLL",
    "OEM_NEC_EQUAL",
    "OEM_FJ_MASSHOU",
    "OEM_FJ_TOUROKU",
    "OEM_FJ_LOYA",
    "OEM_FJ_ROYA",
    "151",
    "152",
    "153",
    "154",
    "155",
    "156",
    "157",
    "158",
    "159",
    "LSHIFT",
    "RSHIFT",
    "LCONTROL",
    "RCONTROL",
    "LMENU",
    "RMENU",
    "BROWSER_BACK",
    "BROWSER_FORWARD",
    "BROWSER_REFRESH",
    "BROWSER_STOP",
    "BROWSER_SEARCH",
    "BROWSER_FAVORITES",
    "BROWSER_HOME",
    "VOLUME_MUTE",
    "VOLUME_DOWN",
    "VOLUME_UP",
    "MEDIA_NEXT_TRACK",
    "MEDIA_PREV_TRACK",
    "MEDIA_STOP",
    "MEDIA_PLAY_PAUSE",
    "LAUNCH_MAIL",
    "LAUNCH_MEDIA_SELECT",
    "LAUNCH_APP1",
    "LAUNCH_APP2",
    "184",
    "185",
    ";",
    "+",
    ",",
    "-",
    ".",
    "/",
    "OEM_3",
    "193",
    "194",
    "195",
    "196",
    "197",
    "198",
    "199",
    "200",
    "201",
    "202",
    "203",
    "204",
    "205",
    "206",
    "207",
    "208",
    "209",
    "210",
    "211",
    "212",
    "213",
    "214",
    "215",
    "216",
    "217",
    "218",
    "[",
    "\\",
    "]",
    "'",
    "OEM_8",
    "224",
    "OEM_AX",
    "OEM_102",
    "ICO_HELP",
    "ICO_00",
    "PROCESSKEY",
    "ICO_CLEAR",
    "PACKET",
    "232",
    "OEM_RESET",
    "OEM_JUMP",
    "OEM_PA1",
    "OEM_PA2",
    "OEM_PA3",
    "OEM_WSCTRL",
    "OEM_CUSEL",
    "OEM_ATTN",
    "OEM_FINISH",
    "OEM_COPY",
    "OEM_AUTO",
    "OEM_ENLW",
    "OEM_BACKTAB",
    "ATTN",
    "CRSEL",
    "EXSEL",
    "EREOF",
    "PLAY",
    "ZOOM",
    "NONAME",
    "PA1",
    "OEM_CLEAR"
};

QueryKeyThread::QueryKeyThread(QObject *parent)
    :QThread(parent)
    ,stopped(false)
{
}

QueryKeyThread::~QueryKeyThread()
{
    requestInterruption();
    quit();
    wait();
}

void QueryKeyThread::run()
{
    BYTE lpKeyState[256] = {0};

    SetKeyboardState(lpKeyState);

    while(!isInterruptionRequested() && !stopped){
        /*获取光标位置*/

        int MAX = 256;
        LPWSTR p_name;

        GUITHREADINFO pg;
        pg.cbSize = sizeof(GUITHREADINFO);
        GetGUIThreadInfo(NULL, &pg);/*获取GUI线程信息*/
        HWND hwnd = pg.hwndCaret;   /*光标窗口句柄*/
        if(pg.hwndCaret)
        {
            p.x = pg.rcCaret.right; /*rcCaret 光标*/
            p.y = pg.rcCaret.bottom;
            ClientToScreen(hwnd, &p);   /*转换为屏幕坐标*/

            if(p_name == nullptr) return;

            int num = GetClassName(pg.hwndCaret, p_name, MAX);    /*获取窗口类名*/

            if(num > 0)
            {
                QString name = ConvertLPWSTRToLPSTR(p_name);

                if("Qt5QWindowIcon" == name)
                {
                    /*编辑器是QtCreator*/
                    p.x += 80;
                }
                else{
                }
            }
        }

        emit mousePointChanged(p);

        for(int i=0;i<256;i++){
            if((GetAsyncKeyState(i) )){
                if(i==0 || i==255) continue;
                if((key_value[i]).contains("BUTTON")) continue;
//                qDebug()<<i<<key_value[i]<<" press"<<GetAsyncKeyState(i);
                emit queryResult(key_value[i]);
            }
        }
        msleep(100);
    }
}

/******************************************************************************************
Function:        ConvertLPWSTRToLPSTR
Description:     LPWSTR转QString
Input:           lpwszStrIn:待转化的LPWSTR类型
Return:          转化后的QString类型
*******************************************************************************************/
QString QueryKeyThread::ConvertLPWSTRToLPSTR(LPWSTR lpwszStrIn)
{
    LPSTR pszOut = NULL;
    try
    {
        if (lpwszStrIn != NULL)
        {
            int nInputStrLen = wcslen(lpwszStrIn);

            // Double NULL Termination
            int nOutputStrLen = WideCharToMultiByte(CP_ACP, 0, lpwszStrIn, nInputStrLen, NULL, 0, 0, 0) + 2;
            pszOut = new char[nOutputStrLen];

            if (pszOut)
            {
                memset(pszOut, 0x00, nOutputStrLen);
                WideCharToMultiByte(CP_ACP, 0, lpwszStrIn, nInputStrLen, pszOut, nOutputStrLen, 0, 0);
            }
        }
    }
    catch (std::exception e)
    {
    }

    return QString(pszOut);
}
