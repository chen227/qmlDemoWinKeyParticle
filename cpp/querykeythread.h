﻿#ifndef QUERYKEYTHREAD_H
#define QUERYKEYTHREAD_H
#include <QThread>

#ifdef Q_OS_WIN
#include <windows.h>
#pragma comment(lib, "user32.lib")
#include <qt_windows.h>
#endif

class QueryKeyThread : public QThread
{
    Q_OBJECT
public:
    QueryKeyThread(QObject* parent=0);
    ~QueryKeyThread();
    void run();

signals:
    void queryResult(QString name);
    void mousePointChanged(POINT p);

private:
    bool stopped;
    POINT p;
    QString ConvertLPWSTRToLPSTR(LPWSTR lpwszStrIn);
};

#endif // QUERYKEYTHREAD_H
