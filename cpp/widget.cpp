﻿#include "widget.h"
#include "ui_widget.h"
#include <QQuickWidget>
#include <QPixmap>
#include <QPalette>
#include <QColor>
#include <QQmlEngine>
#include <QQmlContext>
#include <QDate>
#include <QSettings>
#include <QDir>
#include <QTextStream>
#include <QDesktopWidget>
#include <QApplication>
#include <QDebug>

//GetKeyState() 定时读取状态

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    w(300),
    h(300)
{
    ui->setupUi(this);

    //==================================================================================

    this->setWindowFlags(this->windowFlags() | Qt::SubWindow | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);//窗口置顶，无边框

    setAttribute(Qt::WA_TranslucentBackground);
//    setAttribute(Qt::WA_TransparentForMouseEvents);

    this->setAutoFillBackground(true);//填充背景

    //================================================================================
    QPalette palette;

    palette.setBrush(QPalette::Window, QBrush(QColor(Qt::transparent)));

    this->setPalette(palette);

    this->setFixedSize(getDesktopWidth(),getDesktopHeight());

    //================================================================================
    ui->quickWidget->setClearColor(QColor(Qt::transparent));    //qml 透明

    ui->quickWidget->engine()->rootContext()->setContextProperty("widget",this);

    ui->quickWidget->setSource(QUrl("qrc:/qml/main.qml"));

//    this->setWindowState(Qt::WindowMaximized);

//    this->setFixedSize(getDesktopWidth(),getDesktopHeight()-40);
    this->setFixedSize(w,h);

    setSystemMenu();

    m_thread = new QueryKeyThread(this);
    connect(m_thread,SIGNAL(queryResult(QString)),this,SLOT(getQueryResult(QString)));
    qRegisterMetaType<POINT>("POINT");
    connect(m_thread,SIGNAL(mousePointChanged(POINT)),this,SLOT(get_mousePointChanged(POINT)));
    m_thread->start();

    isHide = false;

    /*鼠标穿透*/
    SetWindowLong((HWND)winId(), GWL_EXSTYLE, GetWindowLong((HWND)winId(), GWL_EXSTYLE) |
                       WS_EX_TRANSPARENT | WS_EX_LAYERED);

    timer = new QTimer(this);
    timer->setSingleShot(true);
    timer->setInterval(200);
    connect(timer, &QTimer::timeout, this, &Widget::sendStop);
}

Widget::~Widget()
{
    delete trayIcon;
    delete ui->quickWidget;
    delete ui;
}

int Widget::getDesktopWidth()
{
    return QApplication::desktop()->width();
}

int Widget::getDesktopHeight()
{
    return QApplication::desktop()->height();
}
//添加系统托盘
void Widget::setSystemMenu()
{
    trayIcon = new QSystemTrayIcon();

    trayIcon->setIcon(QIcon(":/imamges/image/logo.ico"));

    QMenu *menu = new QMenu();

    QAction *action_exit = menu->addAction("Exit");

    connect(action_exit,SIGNAL(triggered(bool)),this,SLOT(systemExit(bool)));

    trayIcon->setContextMenu(menu);

    trayIcon->show();
}
void Widget::systemExit(bool b)
{    
    m_thread->quit();
    m_thread->deleteLater();
    qApp->quit();
}

void Widget::getQueryResult(QString name)
{
    qDebug()<<"getQueryResult = "<<name;
    emit sendKeyPress(name);

    if(timer->isActive())
    {
        timer->stop();
    }
    timer->start();
}

void Widget::get_mousePointChanged(POINT p)
{
    if(last_p.x != p.x || last_p.y != p.y)
    {
        qDebug()<<"get_mousePointChanged = "<<p.x<<p.y;
        last_p = p;
        /*希望窗口在鼠标光标中心*/
        move(p.x - w/2, p.y - h*4/5);
        emit send_mouse_position((int)(p.x), (int)(p.y));
    }
}
