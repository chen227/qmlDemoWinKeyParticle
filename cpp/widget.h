﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QPoint>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>
#include <QAction>
#include "querykeythread.h"
#include <windows.h>
#include <QTimer>

#ifdef Q_OS_WIN
#pragma comment(lib, "user32.lib")
#include <qt_windows.h>
#endif

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void setSystemMenu();
public slots:
    int getDesktopWidth();
    int getDesktopHeight();

    void systemExit(bool b);

    void getQueryResult(QString name);
    void get_mousePointChanged(POINT p);

signals:
    void sendKeyPress(QString name);
    void send_mouse_position(int x, int y);
    void sendStop();
private:
    Ui::Widget *ui;
    QPoint offset;
    QSystemTrayIcon *trayIcon;

    QueryKeyThread* m_thread;
    bool isHide;
    POINT last_p;
    int w;
    int h;
    QTimer *timer;
};

#endif // WIDGET_H
