﻿import QtQuick 2.0
import QtQuick.Particles 2.13

Item {
    id: root
    width: 300
    height: 300

    Connections{
        target: widget
        onSendKeyPress: {
//            console.log("====",name)
            trailsNormal.enabled = true
        }
        onSendStop: {
            trailsNormal.enabled = false
        }
    }

/*

*/

//    Rectangle{
//        anchors.fill: parent
//        color: "transparent"
//        border.color: "red"
//        border.width: 1
//    }

    ParticleSystem { id: sys1 }
    ImageParticle {
        system: sys1
        source: "qrc:/imamges/image/glowdot.png"
        color: "cyan"
        alpha: 0
        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation {
                from: "cyan"
                to: "magenta"
                duration: 1000
            }
            ColorAnimation {
                from: "magenta"
                to: "cyan"
                duration: 1000
            }
        }
        colorVariation: 0.3
    }
    //! [0]
    Emitter {
        id: trailsNormal
        system: sys1
        enabled: false

        emitRate: 60
        lifeSpan: 1000

        y: root.height*4/5
        x: root.width/2

        velocity: PointDirection { x: -30; y: -100; xVariation: 50; yVariation: 80; }
        acceleration: PointDirection {xVariation: 0; yVariation: 0;}
        velocityFromMovement: 10

        size: 4
        sizeVariation: 2
    }
}
