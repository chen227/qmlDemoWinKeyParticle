#-------------------------------------------------
#
# Project created by QtCreator 2017-03-28T09:38:51
#
#-------------------------------------------------

QT       += core gui
QT       += quickwidgets
QT       += qml quick

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qmlDemoWinKeyKaTong
TEMPLATE = app

DESTDIR = $$PWD/bin

RC_FILE += myapp.rc

#QML_INPORT_PATH += "E:/Qt/Qt5.13.0/5.13.0/msvc2017/qml/QtQuick\Particles.2"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp \
    cpp/widget.cpp \
    cpp/querykeythread.cpp

RESOURCES += \
    qml/resource.qrc

FORMS += \
    cpp/widget.ui

HEADERS += \
    cpp/widget.h \
    cpp/querykeythread.h
